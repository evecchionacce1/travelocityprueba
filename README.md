## Proyecto: Travelocity 

Este proyecto es la se realizó con la finalidad de testear las funciones del portal https://www.travelocity.com/ de manera automatizada poara validar los conocimientos de Edanir Vecchionacce en temas de automatizacion.

### Este proyecto cuenta con las siguientes características:

Lenguaje de programación: Java
Framework de Automatización: Serenity
Patrón de diseño: Screenplay
Framework de soporte BDD (Behavior development driver): Cucumber
Lenguaje de BDD (Behavior development driver): Gherkin
Constructor de proyecto: Gradle
Nota: El proyecto está diseñado para ser ejecutado en máquinas Windows y navegador Chrome.



### Estructura del proyecto:

**carpeta funcional** esta capeta contiene os testcase 15 casos de prueba.
**carpeta automatizacion** esta carpeta contiene el proyecto de automatizacion.

**src/main/java/dataBase:** Este paquete contiene las conexiones a base de datos, así como las Querys(esta vacia)

**src/main/java/iteractions:** Este paquete maneja todas las funciones de iteraciones con elementos web, por ejemplo, realizar clic en un elemento web, escribir sobre un elemento web, entre otros

**src/main/java/models:** (esta vacia)

**src/main/java/tasks:** Este paquete maneja las tareas que va a realizar el usuario para cumplir con lo esperado en la prueba automatizada, así cumpliendo con el Patrón de diseño Screenplay

**src/main/java/userinterface:** En este paquete referenciamos los elementos de las vistas con las que vamos a interactuar, así garantizando la reusabilidad de código.

**src/main/java/utils:** En este paquete tenemos los archivos para reutilización de palabras claves, mensajes, entre otras, que están inmersas en el portal  web con el fin de evitar dejar código estático en nuestra automatización

**src/test/java/runner:** Este paquete contiene las clases las cuales ejecutaran nuestra prueba

**src/test/java/stepsdefinitions:** Este paquete contiene nuestros archivos de steps los cuales definen el paso a paso del lado de lógica de programación

**src/test/features:** Este paquete contiene nuestros archivos feature los cuales definen el escenario a evaluar del lado del BDD

**gitignore:** El archivo encargado de bloquear subir archivos no necesarios en nuestro repositorio

**serenity.properties:** Es el archivo que nos ayuda a setear propiedades del Framework de Serenity



### Pasos para obtener el proyecto de automation:

<ul>
<li>Clonar el repositorio en la ubicación deseada, usando el comando: git clone https://gitlab.com/evecchionacce1/travelocityprueba.git
</ul>

### Pasos para ejecutar el proyecto

Se debe primero realizar los pasos de la sección "Pasos para obtener el proyecto de automation"
<ul>
<li>Opción 1: Abrir el proyecto en el IDE de su preferencia que soporte Java, ejecutar cualquiera de las clases que está en la carpeta runner, la cual está por defecto con el  @regresiontest para ejecutar todas las pruebas
<li>Opcion 2: Abrir desde la consola de windows la carpeta donde está clonado el proyecto, la consola debe tener permisos de administrador, ejecutar el comando "gradlew test aggregate"
</ul>

### Generación de reporte del framework
El reporte se generará en la carpeta /target/site/serenity abriendo el archivo index.html

